# Repex-template
## Project template for repeatable scientific machine learning experiments (in Python).

----

<img src="/uploads/f9dd5613aa1926ccb9d459aac3b8ab24/repex_structure.gif" alt="repex_structure" width="50%">

## What is in this repository:
Contents of this repository are meant to be cloned as a baseline for ML projects which can be used as a supplementary material for repeatability/reproducibility of research papers. The structure is very simple on purpose so it can be easily explained (as a teaching-material), and also  so everyone can bend it according to their needs.

* `project.ipynb` - Guided tour through the project. This should be the only file the reader needs to execute in order to understand and fully repeat/reproduce the results which are presented in your white-paper (linking back to this repository). Can contain e.g. exploratory analysis of the data, data preprocessing, outline of the proposed method, configuration of the space of hyper-parameters to search through, presentation of the best results, concluding remarks, timing of the code bottlenecks etc.

* `experiment.py` - Definition of the experiment, e.g. setting seeds for reproducibility, data loading, model definition and export of the results to a specified folder. This file should be runnable separately as a subprocess and configurable through environment variables or arguments. Which makes it easy to run in parallel on e.g. multiple CPUs or GPUs. This file should still be concise and as readable as possible.

* `utils.py` - Module containing auxiliary functions not requiring to be presented and only would clutter the experiment code, reducing the readability.

* `results` - Directory holding the result folders created by each call of the `experiment.py`. The name should be very descriptive for faster navigation in case of numerous experiments with different configuration. This is also a `log_dir` for Tensorboard.

## How to use it:

1. clone this repository
2. fill in `requirements.txt` with dependencies for your project
3. create a virtual environment and install your dependencies
4. activate your virtual environment and fill in empty functions in `utils.py`
5. iteratively fill in the jupyter notebook file to present the workflow
6. define the steps in `experiment.py` and log what you need in `results`
7. run your experiment/s by subprocess call in `project.ipynb`
8. present the results stored in `results` in `project.ipynb`


## Example projects using this structure:

1. https://gitlab.com/paloha-pres/cifar10-cnn
