import os, json

def get_config():
    """
    Function which reads and returns a config for the experiment.
    Either a ENV_VARIABLE parser or ARG_PARSER or file reader which
    returns a dictionary {'config_key': value}
    """
    import os
    config = {
        'SEED': int(os.environ.get('SEED'))
    }
    return config


def get_data():
    """
    Data loader function. Can be a synthetic data generator, generator
    class like keras.Sequence or just a function that loads the data from
    a file on a drive or from a np.memmap etc.
    """
    x = [1,2,3,4,5]
    y = [0,1,0,1,0]
    return x, y

def get_model():
    """
    Definition of the model. Returns compiled model.
    The whole source code of this function can be saved.
    """

def train_model(model):
    """
    Returns a trained model and maybe some logs etc.
    """
    return model, 'Log of the training'


########
# LOGGING helper functions


def log_config(log_dir, config, fname='config.json'):
    """
    Save experiment config to json file
    """
    with open(os.path.join(log_dir, fname), 'w', encoding='utf-8') as f:
        json.dump(config, f, ensure_ascii=False, indent=4)


def log_func_src(log_dir, func, fname=None):
    """
    Save source code of a function to a file
    """
    import inspect
    fname = '{}.py'.format(func.__name__) if fname is None else fname
    path = os.path.join(log_dir, fname)
    source = inspect.getsource(func)
    with open(path, 'w') as outfile:
        print(source, file=outfile)


def log_keras_summary(log_dir, model, fname='summary.txt', mode='a'):
    """
    Save model summary
    """
    def myprint(s):
        with open(os.path.join(log_dir, fname), mode) as f:
            print(s, file=f)
    model.summary(print_fn=myprint)
